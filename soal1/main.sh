#!/bin/bash
#CHECK LOGIN
echo "Welcome! Please login to continue!"
echo -n "Username : "
read username
echo -n "Password : "
read -s password
checkiftrue=false
while [ "$checkiftrue" = false ]
do
  usernamepassword="${username} ${password}"
  if grep -wq "$usernamepassword" ./users/user.txt
  then
    echo "Successfully Login!"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" | sudo tee -a ./log/log.txt
    echo "LOGIN: INFO User $username logged in" | sudo tee -a ./log/log.txt
    checkiftrue=true
  else
    echo "Wrong username or password! Please try again!"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" | sudo tee -a ./log/log.txt
    echo "LOGIN: ERROR Failed login attempt on user $username" | sudo tee -a ./log/log.txt
    echo -n "Username : "
    read username
    echo -n "Password : "
    read -s password
  fi
done

#AFTER LOGIN
echo "Welcome! Please input command!"
read command num

if [[ "$command" = "dl" ]]
then
  if [ -d "$(date +'%Y-%m-%d')_$username" ];
  then
    :
  else
    sudo mkdir ./"$(date +'%Y-%m-%d')_$username"
  fi
  filename="$(date +'%Y-%m-%d')_$username".zip
  if test -f "$filename";
  then
    unzip -q "$filename" -d ./
    totalfile="$(ls ./$(date +'%Y-%m-%d')_$username | wc -l)"
    for ((i=$((totalfile + 1)); i<=$((totalfile + num)); i=i+1))
    do
      if [ $i -lt 10 ]
      then
        sudo wget -qO ./"$(date +'%Y-%m-%d')_$username"/"PIC_0$i" https://loremflickr.com/320/240
      else
        sudo wget -qO ./"$(date +'%Y-%m-%d')_$username"/"PIC_$i" https://loremflickr.com/320/240
      fi
    done
    zip --password "$password" -qr "$filename" ./"$(date +'%Y-%m-%d')_$username"
  else
    for ((i=1; i<=$num; i=i+1))
    do
      if [ $i -lt 10 ]
      then
        sudo wget -qO ./"$(date +'%Y-%m-%d')_$username"/"PIC_0$i" https://loremflickr.com/320/240
      else
        sudo wget -qO ./"$(date +'%Y-%m-%d')_$username"/"PIC_$i" https://loremflickr.com/320/240
      fi
    done
    zip --password "$password" -qr "$filename" ./"$(date +'%Y-%m-%d')_$username"
  fi
elif [[ "$command" = "att" ]]
then
  showupfail="$(grep -ow "LOGIN: ERROR Failed login attempt on user $username" ./log/log.txt | wc -l)"
  showupsuccess="$(grep -ow "LOGIN: INFO User $username logged in" ./log/log.txt | wc -l)"
  showupfile=$((showupsuccess + showupfail))
  echo "There are $showupfile attempts in $username profile"
else
  :
fi 
