#!/usr/bash

directory="/home/graidy/Sisop/soal2/forensic_log_website_daffainfo_log"

#apache server log biasanya di: /var/log/apache2/access.log
#cuman buat contoh make log di directory sendiri aja
log="/home/graidy/Sisop/soal2/log_website_daffainfo.log"

#a
if [ ! -d "$directory" ]; then 
	mkdir $directory
fi

#b
awk 'END{print "Rata-rata serangan adalah sebanyak ",(NR-1)/24," requests per jam"}' $log > $directory/ratarata.txt

#c
awk -F ":" 'BEGIN{max=0} {gsub(/"/, "", $1); if(NR > 1) ip[$1]++; if(ip[$1]>max) max=ip[$1]} END{for(i in ip) if(max==ip[i]) print "IP yang paling banyak mengakses server adalah: ", i, " sebanyak ", max, " requests\n"}' $log > $directory/result.txt

#d
awk 'BEGIN{n_curl=0} /curl/ {n_curl++} END{print "Ada ",n_curl," requests yang menggunakan curl sebagai user-agent\n"}' $log >> $directory/result.txt

#e
awk -F ":" '/22\/Jan\/2022:02/ {gsub(/"/, "", $1); if(NR>1) print $1|"sort -u"}' $log >> $directory/result.txt
