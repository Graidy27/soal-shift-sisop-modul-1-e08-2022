#!/bin/bash
#PROMPTS
checkiftrue=false

if [ -d "./users" ];
then
  :
else
  sudo mkdir ./users
fi
if [ -f "./users/user.txt" ];
then
  :
else
  sudo touch ./users/user.txt
fi
if [ -d "./log" ];
then
  :
else
  sudo mkdir ./log
fi
if [ -f "./log/log.txt" ];
then
  :
else
  sudo touch ./log/log.txt
fi

while [ "$checkiftrue" = false ]
do
  echo "Please enter your new username and password!"
  read newuser newpwd
  if grep -wq "$newuser" ./users/user.txt
  then
    echo "Username already has been registered!"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" | sudo tee -a ./log/log.txt
    echo "REGISTER: ERROR User already exists" | sudo tee -a ./log/log.txt
  else
    checkifpwdtrue=true
    totalchar=$(echo -n "$newpwd" | wc -c)
    has_char_of_each_class() {
      LC_ALL=C awk -- '
        BEGIN {
          for (i = 2; i < ARGC; i++)
            if (ARGV[1] !~ "[[:" ARGV[i] ":]]") exit 1
          }' "$@"
    }

    if has_char_of_each_class "$newpwd" lower upper digit;
    then
      checkifpwdtrue=true
    else
      checkifpwdtrue=false
    fi

    if [[ "$totalchar" -ge 8 && "$checkifpwdtrue" = true ]]
    then
      checkifpwdtrue=true
    else 
      checkifpwdtrue=false
    fi

    if [[ "$newpwd" != "$newuser" && "$checkifpwdtrue" = true ]]
    then
      checkifpwdtrue=true
    else
      checkifpwdtrue=false
    fi

    if [ "$checkifpwdtrue" = true ]
    then
      echo "Username has been registered successfully!"
      echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" | sudo tee -a ./log/log.txt
      echo "REGISTER: INFO User $newuser registered successfully" | sudo tee -a ./log/log.txt
      echo "$newuser $newpwd" | sudo tee -a ./users/user.txt
      checkiftrue=true
    else
      echo "Invalid password!"
    fi
  fi
done

