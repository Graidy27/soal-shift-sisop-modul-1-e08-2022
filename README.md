# Soal Shift Modul 1 Kelompok E08

Anggota :
> Januar Evan - 5025201210\
> Graidy Megananda - 5025201188\
> Helmi Taqiyudin - 5025201152


---
## Tabel Konten
- [Soal 1](#nomor-1)
- [Soal 2](#nomor-2)
  - [Soal 2.a](#2a)
  - [Soal 2.b](#2b)
  - [Soal 2.c](#2c)
  - [Soal 2.d](#2d)
  - [Soal 2.e](#2e)
- [Soal 3](#nomor-3)
  - [Soal 3.a](#a)
  - [Soal 3.b](#b)
  - [Soal 3.c](#c)
  - [Soal 3.d](#d)

## Nomor 1
Cara yang kami pakai adalah sebagai berikut :
> Register.sh
1.  Cek directory pwd ada folder users
    ```shell
    if [ -d "./users" ];
    then
      :
    else
     sudo mkdir ./users
    fi
    ```
2. Cek dalam directory users terdapat user.txt
   ```shell
   if [ -f "./users/user.txt" ];
   then
     :
   else
     sudo touch ./users/user.txt
   fi
   ```
3. Lakukan sama dengan langkah 1 dan 2 tetapi mengecek folder log dan file log.txt dalam folder log
4. Melakukan while input untuk mengecek syarat syarat dari register user
   ```shell
   while [ "$checkiftrue" = false ]
   do
     ...
   done
   ```
5. Dalam while loop, ambil username dan password yang ingin dimasukkan
   ```shell
   echo "Please enter your new username and password!"
   read newuser newpwd
   ```
6. Check dalam ./users/user.txt ada user atau tidak, jika ya, maka keluar dari if statement dan kembali lagi (while loop) dan memasukkan log dalam ./log/log.txt
   ```shell
   if grep -wq "$newuser" ./users/user.txt
   then
     echo "Username already has been registered!"
     echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" | sudo tee -a ./log/log.txt
     echo "REGISTER: ERROR User already exists" | sudo tee -a ./log/log.txt
   else
     ...
   fi
   ```
7. Jika tidak ada user, maka masuk ke else statement dan mengecek password, berikut syarat - syarat password
   > Alphanumeric
   ```shell
   totalchar=$(echo -n "$newpwd" | wc -c)
    has_char_of_each_class() {
      LC_ALL=C awk -- '
        BEGIN {
          for (i = 2; i < ARGC; i++)
            if (ARGV[1] !~ "[[:" ARGV[i] ":]]") exit 1
          }' "$@"
    }
    if has_char_of_each_class "$newpwd" lower upper digit;
    then
      checkifpwdtrue=true
    else
      checkifpwdtrue=false
    fi
    ```

   > Minimal 8 Karakter
   ```shell
   if [[ "$totalchar" -ge 8 && "$checkifpwdtrue" = true ]]
   then
      checkifpwdtrue=true
   else 
      checkifpwdtrue=false
   fi
   ```
   > Password != user
   ```shell
   if [[ "$newpwd" != "$newuser" && "$checkifpwdtrue" = true ]]
   then
      checkifpwdtrue=true
   else
      checkifpwdtrue=false
   fi
   ```
8. Setelah melakukan pengecekan, maka dicek kriteria terpenuhi atau tidak, jika ya, maka dimasukkan user dalam ./users/user.txt dan mengirimkan log dalam ./log/log.txt
   ```shell
   if [ "$checkifpwdtrue" = true ]
    then
      echo "Username has been registered successfully!"
      echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" | sudo tee -a ./log/log.txt
      echo "REGISTER: INFO User $newuser registered successfully" | sudo tee -a ./log/log.txt
      echo "$newuser $newpwd" | sudo tee -a ./users/user.txt
      checkiftrue=true
   else
      echo "Invalid password!"
   fi
   ```
**Bukti user.txt**
![](pic/bukti1.png)
**Bukti log.txt**
![](pic/bukti2.png)
**Bukti register.sh**
![](pic/bukti3.png)
![](pic/bukti4.png)

> Main.sh
1. Melakukan input username dan password
    ```shell
    echo "Welcome! Please login to continue!"
    echo -n "Username : "
    read username
    echo -n "Password : "
    read -s password
    ```
2. Melakukan while loop untuk pengecekan login, jika ya mengirimkan log berhasil, jika tidak while loop dan mengirimkan log gagal
   ```shell
   while [ "$checkiftrue" = false ]
   do
     usernamepassword="${username} ${password}"
     if grep -wq "$usernamepassword" ./users/user.txt
     then
       echo "Successfully Login!"
       echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" | sudo tee -a ./log/log.txt
       echo "LOGIN: INFO User $username logged in" | sudo tee -a ./log/log.txt
       checkiftrue=true
     else
       echo "Wrong username or password! Please try again!"
       echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" | sudo tee -a ./log/log.txt
       echo "LOGIN: ERROR Failed login attempt on user $username" | sudo tee -a ./log/log.txt
       echo -n "Username : "
       read username
       echo -n "Password : "
       read -s password
   fi
   done
   ```
3. Setelah login, read input command yang diambil, att atau dl
   ```shell
   echo "Welcome! Please input command!"
   read command num
   if [[ "$command" = "dl" ]]
   then
     ...
   elif [[ "$command" = "att" ]]
   then
     ...
   else
     :
   fi
   ```
   > dl N 
   1. Check username directory
      ```shell
      if [ -d "$(date +'%Y-%m-%d')_$username" ];
      then
        :
      else
        sudo mkdir ./"$(date +'%Y-%m-%d')_$username"
      fi
   2. Jika ada file directory, maka download gambar kemudian di zip dari banyaknya file dalam folder, jika tidak, dimulai dari 0
      ```shell
      filename="$(date +'%Y-%m-%d')_$username".zip
      if test -f "$filename";
      then
        unzip -q "$filename" -d ./
        totalfile="$(ls ./$(date +'%Y-%m-%d')_$username | wc -l)"
        for ((i=$((totalfile + 1)); i<=$((totalfile + num)); i=i+1))
        do
          if [ $i -lt 10 ]
          then
            sudo wget -qO ./"$(date +'%Y-%m-%d')_$username"/"PIC_0$i" https://loremflickr.com/320/240
          else
            sudo wget -qO ./"$(date +'%Y-%m-%d')_$username"/"PIC_$i" https://loremflickr.com/320/240
          fi
        done
        zip --password "$password" -qr "$filename" ./"$(date +'%Y-%m-%d')_$username"
      else
        for ((i=1; i<=$num; i=i+1))
        do
        if [ $i -lt 10 ]
        then
          sudo wget -qO ./"$(date +'%Y-%m-%d')_$username"/"PIC_0$i" https://loremflickr.com/320/240
        else
          sudo wget -qO ./"$(date +'%Y-%m-%d')_$username"/"PIC_$i" https://loremflickr.com/320/240
        fi
        done
        zip --password "$password" -qr "$filename" ./"$(date +'%Y-%m-%d')_$username"
      fi
      ```
   > att
   Check jumlah username yang ada di dalam log file sesuai dengan username yang sedang login
   ```shell
   showupfail="$(grep -ow "LOGIN: ERROR Failed login attempt on user $username" ./log/log.txt | wc -l)"
   showupsuccess="$(grep -ow "LOGIN: INFO User $username logged in" ./log/log.txt | wc -l)"
   showupfile=$((showupsuccess + showupfail))
   echo "There are $showupfile attempts in $username profile"
   ``` 
**Bukti main.sh**
![](pic/bukti5.png)
![](pic/bukti6.png)
![](pic/bukti7.png)
> Masalah yang dihadapi
1. Tidak bisa menulis file ke dalam log.txt atau users.txt
![](pic/error1.jpg)
2. Mencari command untuk mendownload file
![](pic/error2.jpg)
3. Tidak bisa membuat file user.txt atau log.txt
![](pic/error3.jpg)

## Nomor 2
**[Source Code Soal 2](https://gitlab.com/Graidy27/soal-shift-sisop-modul-1-e08-2022/-/blob/main/soal2/soal2_forensic_dapos.sh)**

**Deskripsi:**\
Menampilkan output tertentu di directory tertentu dari file `log_website_daffainfo.log` menggunakan awk

**Persiapan**
- Variabel `directory` digunakan untuk menyimpan path dari folder `forensic_log_website_daffainfo_log` yang akan digunakan pada beberapa soal
- Variabel `log` digunakan untuk menyimpan path dari access log `log_website_daffainfo.log`


### 2.a
**Deskripsi**\
Membuat folder bernama `forensic_log_website_daffainfo_log`

**Pembahasan**
```
if [ ! -d "$directory" ]; then 
	mkdir $directory
fi
```
- Menggunakan `if [ ! -d "$directory" ]` untuk mengecek apakah folder `forensic_log_website_daffainfo_log` sudah ada atau belum
  - Jika belum, maka folder tersebut akan dibuat menggunakan `mkdir $directory`
  - Jika sudah, maka tidak dilakukan apapun

**Bukti**\
![](pic/Bukti_soal2a.png)

### 2.b
**Deskripsi**\
Menghitung rata-rata penyerangan per jam ke website dapos dan menyimpannya ke dalam file `ratarata.txt` di dalam folder yang sudah dibuat pada 2.a

**Pembahasan**
```
awk 'END{print "Rata-rata serangan adalah sebanyak ",(NR-1)/24," requests per jam"}' $log > $directory/ratarata.txt
```
- `END{...}` -> Segala perintah yang diletakkan di dalam kurung kurawa akan dilaksanakan setelah awk membaca seluruh line pada file access log (log_website_daffainfo.log)
  - `print "Rata-rata serangan adalah sebanyak ",(NR-1)/24," requests per jam"` -> NR merupakan variabel yang menyimpan nomor baris. Karena berada di dalam `END{...}`, maka ia akan menyimpan nomor baris terakhir yang sama artinya dengan banyaknya baris pada input. Karena baris pertama pada access log bukanlah request, maka nilai NR dikurangi 1 terlebih dahulu sebelum dibagi dengan 24 (banyaknya jam dalam 1 hari).
- `> $directory/ratarata.txt` -> Outputnya tidak akan ditampilkan, melainkan disimpan di dalam file benarama `ratarata.txt` di dalam path `directory`

**Bukti**
![](pic/Bukti_soal2b.png)

### 2.c
**Deskripsi**\
Menampilkan IP yang paling banyak melakukan penyerangan dan jumlah penyerangan yang dilakukannya. Output disimpan ke dalam file `result.txt` di dalam folder yang sudah dibuat pada 2.a

**Pembahasan**
```
awk -F ":" 'BEGIN{max=0} {gsub(/"/, "", $1); if(NR > 1) ip[$1]++; if(ip[$1]>max) max=ip[$1]} END{for(i in ip) if(max==ip[i]) print "IP yang paling banyak mengakses server adalah: ", i, " sebanyak ", max, " requests\n"}' $log > $directory/result.txt
```
- `-F ":"` -> Digunakan untuk menetapkan delimiter (pembatas antar kolom) sebagai titik dua (`:`)
- `BEGIN{...}` -> Segala perintah yang diletakkan di dalam kurung kurawa akan dilaksanakan sebelum awk membaca line pada file access log. Digunakan untuk menginisiasi variabel
- `{...}` -> Jika sebelum kurung kurawa tidak ada `BEGIN` atau `END`, maka perintah akan dilakukan untuk setiap line pada access log.
  - `gsub(/"/, "", $1)` -> Digunakan untuk menghapus tanda petik dua pada kolom 1. Jika tidak digunakan, maka outputnya akan menjadi `"..."`
  - `if(NR > 1) ip[$1]++` -> Untuk menghitung banyaknya penyerangan yang dilakukan setiap IP yang unik. Karena baris pertama bukan input, maka dimulai dari baris kedua
  - `if(ip[$1]>max) max=ip[$1]` -> Untuk mencari penyerangan paling banyak dari IP yang ada
- Perintah dibawah ini digunakan untuk mencari IP dengan jumlah penyerangan yang sama dengan yang tersimpan di variabel max. Jika ditemukan, maka IP tersebut pastilah yang melakukan penyerangan paling banyak. 
  ```
  END{for(i in ip) if(max==ip[i]) print "IP yang paling banyak mengakses server adalah: ", i, "  sebanyak ", max, " requests\n"}
  ```
- `> $directory/result.txt` -> Outputnya disimpan di dalam file benarama `result.txt` di dalam path `directory`

### 2.d
**Deskripsi**\
Mencari request yang menggunakan `curl` sebagai user agentnya

**Pembahasan**
```
awk 'BEGIN{n_curl=0} /curl/ {n_curl++} END{print "Ada ",n_curl," requests yang menggunakan curl sebagai user-agent\n"}' $log >> $directory/result.txt
```
- `BEGIN{n_curl=0}` -> Digunakan untuk menginisiasi variabel
- `/curl/` -> Digunakan untuk mencari input (line) yang mengandung string `curl`
- `{n_curl++}` -> Digunakan untuk menghitung banyaknya line yang mengandung string `curl`
- Perintah dibawah ini digunakan untuk menampilkan output
  ```
  END{print "Ada ",n_curl," requests yang menggunakan curl sebagai user-agent\n"}
  ```
- `>> $directory/result.txt` -> Output disimpan (ditambahkan, bukan ditimpa) ke dalam file `result.txt`

### 2.e
**Deskripsi**\
Menampilkan user agent yang menyerang diantara jam 02:00:00 hingga 02:59:59

**Pembahasan**
```
awk -F ":" '/22\/Jan\/2022:02/ {gsub(/"/, "", $1); if(NR>1) print $1|"sort -u"}' $log >> $directory/result.txt
```
- `-F ":"` -> Menetapkan delimiter
- Perintah dibawah ini digunakan untuk mencari reuqest diantara jam 02:00:00 hingga 02:59:59. Caranya dengan mencari line yang mengandung string `22/Jan/2022:02`.
  ```
  /22\/Jan\/2022:02/
  ```
- `gsub(/"/, "", $1)` -> Menghilangkan petik dua pada kolom 1
- `if(NR>1) print $1|"sort -u"}` -> Menampilkan IP (kolom 1) dari line yang mengandung string `22/Jan/2022:02`. `sort -u` digunakan untuk mencegah duplikasi IP
- `>> $directory/result.txt` -> Output disimpan (ditambahkan, bukan ditimpa) ke dalam file `result.txt`

**Bukti 2c hingga 2e**
![](pic/Bukti_soal2c_2d_2e.png)


## Nomor 3

Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.


### A. 
Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.

Sebelum membuat file-file log tersebut, kita terlebih dahulu harus membuat folder untuk menampung file log
```shell
#minute_log.sh
mkdir -p ~/log 
```

Selanjutnya adalah mengambil data-data yang diperlukan dari perintah `free -m` dan `du -sh` menggunakan awk yang kemudian di assign ke suatu variabel untuk memudahkan kita menyimpan data tersebut

```shell
#minute_log.sh
#ambil data free -m line 2
freem1="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7}')" 
#ambil data free -m line 3
freem2="$(free -m | awk '/Swap:/ {printf "%s,%s,%s", $2,$3,$4}')" 
#data disk
disk="$(du -sh ~ | awk '{printf "%s,%s",$2,$1}')" 
```
`/Mem:/` dan `/Swap:/` kita gunakan untuk mencari teks dari perintah `free -m` untuk memudahkan kita mengambil data
Setelah seluruh data terkumpu, kita print seluruh data yang terkumpul,

```shell
#minute_log.sh
printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n$freem1,$freem2,$disk" > ~/log/"metrics_ $(date +"%Y%m%d%H%M%S").log"
```

### B.
Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.

```shell
#minute_log.sh
if ! crontab -l | grep -q 'minute_log.sh'; then
    crontab -l | { cat; echo "* * * * * bash ~/soal-shift-sisop-modul-1-e08-2022/soal3/minute_log.sh"; } | crontab -
fi
```
Sebelum membuat printah cron, kita perlu mengecek apakah perintah untuk menjalankan file minute.log sudah ada menggunakan `grep -q` agar perintah cron tidak menumpuk. 
Kemudian jika tidak ada, barulah kita tulis perintah cron dari `crontab -l`

### C.
Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log

untuk membuat file agregasi 1 jam sebelumnya, pertama kita perlu memfilter file dalam rentang waktu tersebut menggunakan:
```shell
jamsebelum=$(date -d '-1 hour' +"%Y%m%d%H")
```

```shell
function count(num1, num2)
{
	return num1 + num2
}
```
selanjutnya, kita menggunakan fungsi count yang menjumlahkan 2 angka untuk memudahkan kita dalam perhitungan rata - rata 

```shell
BEGIN { n++ }
FNR%2==0 { for(i=1; i<=NF; i++) arr[n, i] = $i ; n++ }
END { 
	for(i=1; i<=NF; i++) 
	{
    	for(j=1; j<n; j++) 
	{
    newArr[j] = arr[j, i]
    sum[i] = count(sum[i], arr[j, i])
	}
	asort(newArr)
    min[i] = newArr[1]
    max[i] = newArr[n-1]
    }
    n--
	
	printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

    printf("\nminimum")
    for (i in min) printf(",%s", min[i])

    printf("\nmaximum")
    for (i in max) printf(",%s", max[i])
    
    printf("\naverage")
    for (i=1; i<NF-1; i++) printf(",%s", sum[i]/n)
    printf(",%s,%sM", min[NF-1], sum[NF]/n)

}' ~/log/metrics_$jamsebelum*.log > ~/log/metrics_agg_$jamsebelum.log 
```
kita lanjutkan dengan mengambil data-data dari file log permenit dalam interval 1 jam sebelum untuk dijadikan log agregasi, operasi yang dilakukan disini adalah memindahkan data - data dari file ke sebuah array sekaligus menjumlahkannya untuk memudahkan menghitung rata-rata. setelah menjadi array kita gunakan fungsi `asort` untuk mencari nilai min dan max dari data-data tersebut. Lalu, untuk mencari rata-rata kita menggunakan data yang sudah dijumlah tadi dan membaginya dengan `n`/jumlah file. Setelah seluruh data terkumpul, kita print data tersebut pada log agregasi.

### D.
Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

```shell
#minute_log.sh
chmod -R 700 ~/log
```
dengan menggunakan chmod -R 700 hanya user yang dapat menulis, membaca, dan mengeksekusi perintah pada folder log
### Hasil

![image](/uploads/506428b79f50cd4aadca16f53b3b9118/image.png)

### Kendala
Kendala pada sorting di 3C, awalnya ingin menggunakan fungsi sorting,
```shell
function sorting(arr) 
{
	for (i = 1; i < length(arr); i++) 
	{
		for (j = i; j > 0 && arr[j] < arr[j-1]; j--) 
		{
			temp = arr[j]
			arr[j] = arr[j-1]
			arr[j-1] = temp
		}
	}
		return arr
}

function count(num1, num2)
{
	return num1 + num2
}
```
tetapi saat di run menampilkan error
```
helmitaqiyudin@DESKTOP-QBVFTHC:~$ bash aggregate_minutes_to_hourly_log.sh
awk: cmd. line:14: (FILENAME=/home/helmitaqiyudin/log/metrics_20220306175852.log FNR=2) fatal: attempt to use array `arr (from newArr)' in a scalar context
```

