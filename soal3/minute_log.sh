#!/bin/sh

#buat directory
mkdir -p ~/log 

#ambil data free -m line 2
freem1="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7}')" 
#ambil data free -m line 3
freem2="$(free -m | awk '/Swap:/ {printf "%s,%s,%s", $2,$3,$4}')" 
#data disk
disk="$(du -sh ~ | awk '{printf "%s,%s",$2,$1}')" 

#print minute log
printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n$freem1,$freem2,$disk" > ~/log/"metrics_$(date +"%Y%m%d%H%M%S").log"

#chmod agar hanya user yang dapat mengakses 
chmod -R 700 ~/log

#crontab
if ! crontab -l | grep -q 'minute_log.sh'; then
    crontab -l | { cat; echo "* * * * * bash ~/soal-shift-sisop-modul-1-e08-2022/soal3/minute_log.sh"; } | crontab -
fi