#!/bin/bash

jamsebelum=$(date -d '-1 hour' +"%Y%m%d%H")
awk -F "," '

function sorting(arr) 
{
	for (i = 1; i < length(arr); i++) 
	{
		for (j = i; j > 0 && arr[j] < arr[j-1]; j--) 
		{
			temp = arr[j]
			arr[j] = arr[j-1]
			arr[j-1] = temp
		}
	}
		return arr
}

function count(num1, num2)
{
	return num1 + num2
}

BEGIN { n++ }
FNR%2==0 { for(i=1; i<=NF; i++) arr[n, i] = $i ; n++ }
END { 
	for(i=1; i<=NF; i++) 
	{
    	for(j=1; j<n; j++) 
	{
    newArr[j] = arr[j, i]
    sum[i] = count(sum[i], arr[j, i])
	}
	asort(newArr)
    min[i] = newArr[1]
    max[i] = newArr[n-1]
    }
    n--
	
	printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

    printf("\nminimum")
    for (i in min) printf(",%s", min[i])

    printf("\nmaximum")
    for (i in max) printf(",%s", max[i])
    
    printf("\naverage")
    for (i=1; i<NF-1; i++) printf(",%s", sum[i]/n)
    printf(",%s,%sM", min[NF-1], sum[NF]/n)

}' ~/log/metrics_$jamsebelum*.log > ~/log/metrics_agg_$jamsebelum.log 

if ! crontab -l | grep -q 'aggregate_minutes_to_hourly_log.sh'; then
    crontab -l | { cat; echo "0 * * * * bash ~/soal-shift-sisop-modul-1-e08-2022/soal3/aggregate_minutes_to_hourly_log.sh"; } | crontab -
fi
